# Add your requirements here like:
six
numpy>=1.4
scipy>=0.9
matplotlib
pytest
xlrd
xlwt
openpyxl
h5py
pandas
tables
future
paramiko
psutil
